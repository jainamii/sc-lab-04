package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Gui extends JFrame{
	JPanel frame1,frame2;
	JFrame f1,f2;
	JComboBox<String> jcombo;
	JTextField size;
	JButton button;
	JLabel namesize,nameselect;
	Controller c;
	JTextArea textArea;
	String result = "";
	public Gui(){
		c = new Controller();
		frame1 = new JPanel();
		frame2 = new JPanel();
		textArea = new JTextArea();
		JScrollPane scroll = new JScrollPane (textArea);
		
		f1 = new JFrame();
		f2 = new JFrame();
		
		jcombo = new JComboBox<String>();
		jcombo.setBounds(110, 30, 250, 30);
		frame1.add(jcombo);
		
		size = new JTextField();
		size.setBounds(110,90, 200, 25);
		frame1.add(size); 
		
		button = new JButton("Total");
		button.setBounds(0, 127, 395, 40);
		frame1.add(button);
		
		nameselect = new JLabel("Style : ");
		nameselect.setBounds(50, 20, 100, 50);
		frame1.add(nameselect);
		
		namesize = new JLabel("Size :");
		namesize.setBounds(50, 75, 50, 50);
		frame1.add(namesize);
		
		jcombo.addItem("Style 1");
		jcombo.addItem("Style 2");
		jcombo.addItem("Style 3");
		jcombo.addItem("Style 4");

		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				result+=c.outPut(jcombo.getSelectedIndex(),size.getText());
				textArea.setText(result);
			}
		});
		
		frame1.setLayout(null);

		
		f1.add(frame1);
		f2.add(frame2);
		
		f1.setVisible(true);
		f2.setVisible(true);
		
		f1.setSize(400,200);
		f1.setLocation(500,200);
		f1.setResizable(false);
		
		f2.setSize(400,400);
		f2.setLocation(50,50);
		
		f2.add(scroll);
	}

}
