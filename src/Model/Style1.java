package Model;

public class Style1
{
	private int size;
	
	public Style1(int size)
	{
		this.size = size;

	}
	
	@Override
	public String toString() 
	{
		String r = "";
		for (int i = 1 ; i <= 3*size ; i++)
		{
			for (int j = 1 ; j <= 4*size ; j++)
				r = r + "*";
			r = r + "\n";
		}
		return r ;
	}
}

	
