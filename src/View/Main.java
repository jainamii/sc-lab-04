package View;


import javax.swing.JFrame;

import Controller.Gui;

public class Main {
	public static void main(String[] args){
		JFrame frame = new Gui();
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
